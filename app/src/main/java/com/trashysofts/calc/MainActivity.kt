package com.trashysofts.calc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        bPlus.setOnClickListener {

        }
        bMinus.setOnClickListener {

        }
        bMultiply.setOnClickListener {

        }
        bDivision.setOnClickListener {

        }
        bDel.setOnClickListener {
            deleteFun()
        }
        bDot.setOnClickListener {

        }
        bEqual.setOnClickListener {

        }
        b0.setOnClickListener {
            writeText(b0)
        }
        b1.setOnClickListener {
            writeText(b1)
        }
        b2.setOnClickListener {
            writeText(b2)
        }
        b3.setOnClickListener {
            writeText(b3)
        }
        b4.setOnClickListener {
            writeText(b4)
        }
        b5.setOnClickListener {
            writeText(b5)
        }
        b6.setOnClickListener {
            writeText(b6)
        }
        b7.setOnClickListener {
            writeText(b7)
        }
        b8.setOnClickListener {
            writeText(b8)
        }
        b9.setOnClickListener {
            writeText(b9)
        }

    }
    private fun writeText(button: Button){
        operationOutput.text = operationOutput.text.toString() + button.text.toString()
    }
    private fun deleteFun(){
        if (operationOutput.text.isNotEmpty())
            operationOutput.text =  operationOutput.text.toString().substring(0, operationOutput.text.toString().length-1)
    }

}
